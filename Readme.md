## Placing Object in Plane

# Preview:

![alt text](https://gitlab.com/rahulmool/unity-ar-object-placement/-/raw/master/readmedata/img1.jpeg)

# Tutorial Link:

[![IMAGE ALT TEXT HERE](https://i.ytimg.com/vi/VMjZ70PmnPs/maxresdefault.jpg)](https://youtu.be/VMjZ70PmnPs)

[AR Foundation Object Placement - Unity Augmented Reality/AR](https://youtu.be/VMjZ70PmnPs)